import { expect } from 'chai';
import 'mocha';
import { fizzbuzz, isLeapYears } from '../main';

describe(
  'FizzBuzz tests',
  () => {
    it("it should return input number if not divisible by 3 or 5", () => {
      const result = fizzbuzz(14);
      console.log(result);
      expect(result).to.equal(14);
    })

    it("it should return Fizz if divisible by 3", () => {
      const result = fizzbuzz(3);
      expect(result).to.equal('Fizz');
    })

    it("it should return Buzz if divisible by 5", () => {
      const result = fizzbuzz(5);
      expect(result).to.equal('Buzz');
    })

    it("it should return FizzBuzz if divisible by 3 and 5", () => {
      const result = fizzbuzz(15);
      expect(result).to.equal('FizzBuzz');
    })
  }
)

describe(
  'LeapYears tests',
  () => {
    it("it should return true if year is divisible by 400", () => {
      const result = isLeapYears(2000);
      expect(result).to.be.true;
    })

    it("it should return false if year is divisible by 100 but not by 400", () => {
      const result = isLeapYears(1700);
      expect(result).to.be.false;
    })

    it("it should return true if year is divisible by 4 but not by 100", () => {
      const result = isLeapYears(2008);
      expect(result).to.be.true;
    })

    it("it should return false if year is not divisible by 4", () => {
      const result = isLeapYears(2019);
      expect(result).to.be.false;
    })
  }
)
