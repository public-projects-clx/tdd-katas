export const fizzbuzz = (value: number): string | number => {
  return (value % 3 ? '' : 'Fizz') + (value % 5 ? '' : 'Buzz') || value
}

export const isLeapYears = (year: number) => {
  return !(year % 400) || (!(year % 4) && (year % 100)) ? true : (!(year % 100) && (year % 400)) || (year % 4) ? false : false;
}
